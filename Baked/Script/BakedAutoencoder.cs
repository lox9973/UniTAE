﻿using UnityEngine;
#if UDON
using Graphics = VRC.SDKBase.VRCGraphics;
#endif

namespace UniTAE {
#if !UDON
public class BakedAutoencoder : MonoBehaviour
#else
[UdonSharp.UdonBehaviourSyncMode(UdonSharp.BehaviourSyncMode.None)]
public class BakedAutoencoder : UdonSharp.UdonSharpBehaviour
#endif
{
	public Material[] materials;
	public Texture inputTexture;
	public RenderTexture outputTexture;
	private RenderTexture[] targets;
	private Vector2[] scales;
	private Vector2 rtScale;
	private Vector2 outScale;
	private float[] costs;
	private float totalCost;

	void LoadPasses() {
		var size0 = Vector2.one;
		targets = new RenderTexture[materials.Length];
		scales = new Vector2[materials.Length];
		costs = new float[materials.Length];
		totalCost = 0f;
		rtScale = Vector2.zero;
		for(int i=0; i<materials.Length; i++) {
			var dim = materials[i].GetVector("_InputDim");
			if(i == 0)
				size0 = (Vector2)dim;
			if(i < materials.Length-1)
				targets[i] = (RenderTexture)materials[i].GetTexture("_OutputTex");
			scales[i] = new Vector2(dim.x/size0.x, dim.y/size0.y);
			rtScale = Vector2.Max(rtScale, scales[i] *
				new Vector2(Mathf.Min(dim.z, 4f), Mathf.CeilToInt(dim.z / 4f)));
			costs[i] = scales[i].x*scales[i].y * dim.z * (1f+dim.w);
			totalCost += costs[i];
		}
		outScale = scales[materials.Length-1] / scales[1];
	}
	void SetupParams() {
		const int maxRTSize = 8192;
		var width = Mathf.Min(inputTexture.width, Mathf.Floor(maxRTSize/rtScale.x));
		var height = Mathf.Min(inputTexture.height, Mathf.Floor(maxRTSize/rtScale.y));
		SetRTSize(outputTexture, Mathf.CeilToInt(width*outScale.x), Mathf.CeilToInt(height*outScale.y));
		for(int i=0; i<materials.Length-1; i++)
			SetRTSize(targets[i], Mathf.CeilToInt(width*rtScale.x), Mathf.CeilToInt(height*rtScale.y));

		materials[0].SetTexture("_InputTex", inputTexture);
		for(int i=0; i<materials.Length; i++) {
			var dim = materials[i].GetVector("_InputDim");
			dim.x = Mathf.Round(width * scales[i].x);
			dim.y = Mathf.Round(height * scales[i].y);
			materials[i].SetVector("_InputDim", dim);
		}
		targets[materials.Length-1] = outputTexture;
	}
	void SetRTSize(RenderTexture rt, int width, int height) {
		if(rt.width == width && rt.height == height)
			return;
		rt.Release();
		rt.width = width;
		rt.height = height;
	}

	public bool loop = true;
	public float duration = 1f;
	private int cursor;
	private float reservoir;
	void Restart() {
		cursor = 0;
		reservoir = 0f;
	}
	bool Execute() {
		if(cursor >= materials.Length)
			return false;

		var dt = Mathf.Min(Time.smoothDeltaTime, 1f/60);
		reservoir += dt * (totalCost / duration);
		while(reservoir >= 0f) {
			Graphics.Blit(null, targets[cursor], mat:materials[cursor], pass:0);
			reservoir -= costs[cursor];
			cursor ++;
			if(cursor >= materials.Length) {
				Finish();
				return true;
			}
		}
		return true;
	}
	void Finish() {
		if(loop)
			cursor = 0;
		else
			for(int i=0; i<materials.Length-1; i++)
				targets[i].Release();
	}

	public void OnEnable() {
		LoadPasses();
		SetupParams();
		Restart();
	}
	public void Update() {
		if(!Execute())
			this.enabled = false;
	}
}
}