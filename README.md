# UniTAE

This is a port of [Tiny AutoEncoder for Stable Diffusion](https://github.com/madebyollin/taesd) in Unity pixel shaders. Both taesd_encoder and taesd_decoder are implemented.

To compress an image in python, run `taesd.py YOUR_IMAGE.png` from the taesd repository, and it will generate a compressed image called `YOUR_IMAGE.png.encoded.png`.