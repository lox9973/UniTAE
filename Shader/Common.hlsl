#define RenderTexture3D Texture2D

uint4 LoadRT(RenderTexture3D<uint4> tex, uint3 upos, uint3 size) {
	return tex[upos.xy + size.xy * uint2(upos.z%4, upos.z/4)];
}
uint4 LoadRTPadZero(RenderTexture3D<uint4> tex, int3 ipos, uint3 size) {
	return all(ipos.xy >= 0 && ipos.xy < int2(size.xy)) ? LoadRT(tex, ipos, size) : 0;
}
uint3 GetPosRT(float2 screenPos, uint3 size) {
	uint2 pos = floor(screenPos.xy);
	uint2 grid = pos / size;
	return uint3(pos % size, grid.x >= 4 ? 4096 : dot(grid, uint2(1, 4)));
}
void UnpackF16(out half4 lo, out half4 hi, uint4 x) {
	lo = f16tof32(x);
	hi = f16tof32(x >> 16);
}
uint4 PackF16(half4 lo, half4 hi) {
	return f32tof16(lo) | (f32tof16(hi) << 16);
}

void vertQuad(float2 uv : TEXCOORD0, out float4 vertex : SV_Position) {
	vertex = float4(uv*2-1, UNITY_NEAR_CLIP_VALUE, 1);
}