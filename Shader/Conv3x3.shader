Shader "UniTAE/Conv3x3" {
Properties {
	[HideInInspector] _OutputTex("_OutputTex", 2D) = "black" {} // reference storage

	_InputDim("_InputDim", Vector) = (512, 1024, 16, 16)
	[NoScaleOffset] _InputTex("_InputTex", 2D) = "black" {}
	[NoScaleOffset] _WeightTex("_WeightTex", 2DArray) = "black" {}
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
HLSLINCLUDE
#include "UnityCG.cginc"
#include "Common.hlsl"

RenderTexture3D<uint4> _InputTex; uint4 _InputDim;
#if defined(PACKED_WEIGHT)
Texture2DArray<uint4> _WeightTex;
#else
Texture2DArray<half4> _WeightTex;
#endif

uint4 main(uint3 pos) {
	half4 Y0 = 0;
	half4 Y1 = 0;
	[loop] for(uint k=0; k<_InputDim.z; k++)
	[unroll] for(int j=0; j<3; j++) {
		half4 X[6], A0, A1;
		UnpackF16(X[0], X[1], LoadRTPadZero(_InputTex, int3(int2(pos.xy)+int2(-1,j-1), k), _InputDim.xyz));
		UnpackF16(X[2], X[3], LoadRTPadZero(_InputTex, int3(int2(pos.xy)+int2( 0,j-1), k), _InputDim.xyz));
		UnpackF16(X[4], X[5], LoadRTPadZero(_InputTex, int3(int2(pos.xy)+int2(+1,j-1), k), _InputDim.xyz));
		[unroll] for(int i=0; i<3; i++)
		[unroll] for(int l=0; l<2; l++) {
#if defined(PACKED_WEIGHT) // test shows weight-packing has negligible speedup
			UnpackF16(A0, A1, _WeightTex[uint3(k*2+l, i+j*3, pos.z)]);
#else
			A0 = _WeightTex[uint3(k*4+(l*2+0), i+j*3, pos.z)];
			A1 = _WeightTex[uint3(k*4+(l*2+1), i+j*3, pos.z)];
#endif
			Y0 += A0*X[i+1][l*2+0];
			Y1 += A0*X[i+2][l*2+0];
			Y0 += A1*X[i+1][l*2+1];
			Y1 += A1*X[i+2][l*2+1];
		}
	}
	return PackF16(Y0, Y1);
}
uint4 frag(float4 screenPos : SV_Position) : SV_Target {
	uint3 pos = GetPosRT(screenPos, _InputDim.xyw);
	if(any(pos >= _InputDim.xyw))
		discard;
	return main(pos);
}
ENDHLSL
	Pass {
		Cull Off
HLSLPROGRAM
#pragma target 5.0
#pragma vertex vertQuad
#pragma fragment frag
#pragma shader_feature _ PACKED_WEIGHT
ENDHLSL
	}
}
}