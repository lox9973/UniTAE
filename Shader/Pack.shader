Shader "UniTAE/Pack" {
Properties {
	[HideInInspector] _OutputTex("_OutputTex", 2D) = "black" {} // reference storage

	_InputDim("_InputDim", Vector) = (1024, 1024, 1, 0)
	[NoScaleOffset] _InputTex("_InputTex", 2D) = "black" {}
	[ToggleUI] _Gamma("_Gamma", Int) = 1
	_Scale("_Scale", Vector) = (1, 1, 1, 1)
	_Offset("_Offset", Vector) = (0, 0, 0, 0)
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
HLSLINCLUDE
#include "UnityCG.cginc"
#include "Common.hlsl"

SamplerState sampler_InputTex;
Texture2D<half4> _InputTex; uint3 _InputDim;
uint _Gamma;
half4 _Scale, _Offset;

uint4 main(uint3 pos) {
	pos.y = _InputDim.y-1 - pos.y; // independent of _ProjectionParams.x

	half4 X0 = _InputTex.SampleLevel(sampler_InputTex, (float2(pos.x*2+0, pos.y)+0.5)/_InputDim.xy, 0);
	half4 X1 = _InputTex.SampleLevel(sampler_InputTex, (float2(pos.x*2+1, pos.y)+0.5)/_InputDim.xy, 0);
	if(_Gamma) {
		X0.x = LinearToGammaSpaceExact(X0.x);
		X0.y = LinearToGammaSpaceExact(X0.y);
		X0.z = LinearToGammaSpaceExact(X0.z);
		X1.x = LinearToGammaSpaceExact(X1.x);
		X1.y = LinearToGammaSpaceExact(X1.y);
		X1.z = LinearToGammaSpaceExact(X1.z);
	}
	half4 Y0 = X0 * _Scale + _Offset;
	half4 Y1 = X1 * _Scale + _Offset;
	return PackF16(Y0, Y1);
}
uint4 frag(float4 screenPos : SV_Position) : SV_Target {
	uint3 pos = GetPosRT(screenPos, uint3(_InputDim.x/2, _InputDim.yz));
	if(any(pos >= uint3(_InputDim.x/2, _InputDim.yz)))
		discard;
	return main(pos);
}
ENDHLSL
	Pass {
		Cull Off
HLSLPROGRAM
#pragma target 5.0
#pragma vertex vertQuad
#pragma fragment frag
ENDHLSL
	}
}
}