Shader "UniTAE/Scalar" {
Properties {
	[HideInInspector] _OutputTex("_OutputTex", 2D) = "black" {} // reference storage

	_Upsample("_Upsample", Int) = 1
	_Downsample("_Downsample", Int) = 1
	_InputDim("_InputDim", Vector) = (512, 1024, 16, 0)
	[NoScaleOffset] _InputTex("_InputTex", 2D) = "black" {}
	[NoScaleOffset] _BiasTex("_BiasTex", 2D) = "black" {}
	[NoScaleOffset] _AddTex("_AddTex", 2D) = "black" {}
	_Bias("Bias", Int) = 0
	_Add("Add", Int) = 0
	[Enum(None,0,ReLU,1)] _Act("Act", Int) = 0
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
HLSLINCLUDE
#include "UnityCG.cginc"
#include "Common.hlsl"

RenderTexture3D<uint4> _InputTex; uint3 _InputDim; uint _Upsample, _Downsample;
Texture2D<half4> _BiasTex; uint _Bias;
RenderTexture3D<uint4> _AddTex; uint _Add;
uint _Act;

uint4 main(uint3 pos) {
	half4 X[2], tmp;
	if(_Downsample == 2) {
		UnpackF16(X[0], tmp, LoadRT(_InputTex, uint3(pos.xy*_Downsample+uint2(0,0), pos.z), _InputDim.xyz));
		UnpackF16(X[1], tmp, LoadRT(_InputTex, uint3(pos.xy*_Downsample+uint2(1,0), pos.z), _InputDim.xyz));
	} else {
		UnpackF16(X[0], X[1], LoadRT(_InputTex, uint3(pos.xy/_Upsample, pos.z), _InputDim.xyz));
		if(_Upsample == 2)
			X[0] = X[1] = X[pos.x&1];
	}
	half4 Y0 = X[0], Y1 = X[1];
	if(_Bias) {
		half4 bias = _BiasTex[uint2(pos.z, 0)];
		Y0 += bias;
		Y1 += bias;
	}
	if(_Add) {
		half4 A0, A1;
		UnpackF16(A0, A1, LoadRT(_AddTex, pos.xyz, _InputDim.xyz)); // assume no up/downsample
		Y0 += A0;
		Y1 += A1;
	}
	if(_Act == 1) {
		Y0 = max(0, Y0);
		Y1 = max(0, Y1);
	}
	return PackF16(Y0, Y1);
}
uint4 frag(float4 screenPos : SV_Position) : SV_Target {
	uint3 pos = GetPosRT(screenPos, uint3(_InputDim.xy*_Upsample/_Downsample, _InputDim.z));
	if(any(pos >= uint3(_InputDim.xy*_Upsample/_Downsample, _InputDim.z)))
		discard;
	return main(pos);
}
ENDHLSL
	Pass {
		Cull Off
HLSLPROGRAM
#pragma target 5.0
#pragma vertex vertQuad
#pragma fragment frag
ENDHLSL
	}
}
}