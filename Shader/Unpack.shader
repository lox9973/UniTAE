Shader "UniTAE/Unpack" {
Properties {
	_InputDim("_InputDim", Vector) = (512, 1024, 1, 0)
	[NoScaleOffset] _InputTex("_InputTex", 2D) = "black" {}
	[ToggleUI] _Gamma("_Gamma", Int) = 1
	_Scale("_Scale", Vector) = (1, 1, 1, 1)
	_Offset("_Offset", Vector) = (0, 0, 0, 0)
}
SubShader {
	Tags { "PreviewType"="Plane" } // prevent freezing Unity editor
HLSLINCLUDE
#include "UnityCG.cginc"
#include "Common.hlsl"

RenderTexture3D<uint4> _InputTex; uint3 _InputDim;
uint _Gamma;
half4 _Scale, _Offset;

half4 main(uint3 pos) {
	pos.y = _InputDim.y-1 - pos.y; // independent of _ProjectionParams.x

	half4 X[2];
	UnpackF16(X[0], X[1], LoadRT(_InputTex, uint3(pos.x/2, pos.yz), _InputDim.xyz));
	half4 Y = X[pos.x&1];
	Y = Y * _Scale + _Offset;
	if(_Gamma) {
		Y.rgb = saturate(Y.rgb);
		Y.x = GammaToLinearSpaceExact(Y.x);
		Y.y = GammaToLinearSpaceExact(Y.y);
		Y.z = GammaToLinearSpaceExact(Y.z);
	}
	return Y;
}
half4 frag(float4 screenPos : SV_Position) : SV_Target {
	uint3 pos = GetPosRT(screenPos, uint3(_InputDim.x*2, _InputDim.yz));
	if(any(pos >= uint3(_InputDim.x*2, _InputDim.yz)))
		discard;
	return main(pos);
}
ENDHLSL
	Pass {
		Cull Off
HLSLPROGRAM
#pragma target 5.0
#pragma vertex vertQuad
#pragma fragment frag
ENDHLSL
	}
}
}